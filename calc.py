from bottle import (
    route, run, get, post, static_file,
    template)
import operator as op

@route('/')
def index():
    fruits = ['Apple', 'Banana', 'Coconut', 'Dragonfruit']
    return template('index', name='Tunde',
        items=fruits)

@route('/<username>')
def user_tweets(username):
    pass

@route('/following')
def user_following():
    pass

@route('/followers')
def user_followers():
    pass

@route('/favorites')
def favorites():
    pass

@route('/<username>/lists')
def users_lists(username):
    pass
    
@route('/hashtag/<hashtag>')
def hashtags(hashtag):
    pass


@route('/today')
def today():
    return 'Wednesday Jun 24, 2015'
    
@route('/yesterday')
def yesterday():
    return 'Tuesday Jun 23, 2015'
    
@route('/user/login')
def login():
    pass

@route('/user/profile')
def profile():
    pass

@route('/user/<username>/messages')
def messages(username):
    pass

@route('/gt/<a:int>/<b:int>')
def gt(a, b):
    return str(a > b)

@route('/<operation>/<a:int>/<b:int>')
def calculator(operation, a, b):
    operations = {'add': op.add,
        'sub': op.sub, 'mul': op.mul, 'div': op.truediv,
        'pow': op.pow}
    try:
        answer = operations[operation](a, b)
    except KeyError:
        answer = 'error'
    return str(answer)

@route('/hello/<name>')
def hello(name):
    return template('hello', name=name, items=[])

@route('/static/<filepath:path>')
def static(filepath):
    return static_file(filepath, 'media')

if __name__ == '__main__':
    run(host='localhost', port=8000)
